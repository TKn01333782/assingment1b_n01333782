﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace gym_membership
{

    public partial class gym : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void submit(object sender, EventArgs e)
        {
            if (!Page.IsValid)
            {
                return;
            }

            string type = typeofmembership.SelectedItem.Text.ToString();

            Membership newmembership = new Membership(type);
                
            string Name = nametextbox.Text.ToString();
            string Email = emailidtextbox.Text.ToString();
            string Adress = Address.Text.ToString();
            Client newclient = new Client();
            newclient.ClientName = Name;
            newclient.ClientAdress = Adress;
            newclient.ClientEmail = Email;

            submit sd = new submit(newclient, newmembership);
            receipt.InnerHtml=sd.PrintConfirmation();



        }

    }
}