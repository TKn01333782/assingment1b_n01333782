﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="gym.aspx.cs" Inherits="gym_membership.gym" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head runat="server">
    <title>XYZ GYM</title>
    </head>

    <body>
        <h1>Best Gym in Town</h1>

    <form id="form1" runat="server">
        <div>
      
            <asp:Label runat="server" ID="firstName">First Name</asp:Label>
            <asp:TextBox runat="server" ID="nametextbox" placeholder="e.g.john abc"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a Name" ControlToValidate="nametextbox" ID="validatorName"></asp:RequiredFieldValidator>
            <br /><br />
            <asp:Label runat="server" ID="lastname">Last Name</asp:Label>
            <asp:TextBox runat="server" ID="lastnametextbox"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a  Last Name" ControlToValidate="lastnametextbox" ID="RequiredFieldValidator1"></asp:RequiredFieldValidator>
            <br /><br />
            <asp:Label runat="server" ID="addres">Address </asp:Label>
            <asp:TextBox runat="server" ID="Address" placeholder="Address"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please Provide Address" ControlToValidate="Address" ID="RequiredFieldValidator2"></asp:RequiredFieldValidator>
            <br />
            <asp:Label runat="server" ID="postalcode">Postal Code</asp:Label>
            <asp:TextBox runat="server" ID="postalcodetextbox" Placeholder="L4T1D6"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter Postal Code" ControlToValidate="postalcodetextbox" ID="RequiredFieldValidator3"></asp:RequiredFieldValidator>
            <br /><br />
            <asp:Label runat="server" ID="clientemail">Please Entre Customer Email Id</asp:Label>
            <asp:TextBox runat="server" ID="emailidtextbox" placeholder="E.g.xyz@abc.com"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter an Email" ControlToValidate="emailidtextbox" ID="RequiredFieldValidator4"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="validEmail" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="emailidtextbox" ErrorMessage="Invalid Email Format"></asp:RegularExpressionValidator>
            <br /><br />

            <asp:Label runat="server" ID="membership">Type of Membership</asp:Label>
            <asp:DropDownList runat="server" ID="typeofmembership">
                <asp:ListItem value="S" Text="silver"></asp:ListItem>
                <asp:ListItem Value="g" Text="gold"></asp:ListItem>
                <asp:ListItem Value="P" Text="Platinum"></asp:ListItem>
            </asp:DropDownList>        
            <br /><br />

            <asp:RadioButtonList runat="server" ID="paymentType">
                <asp:ListItem Text="Monthly" >Monthly</asp:ListItem>
                <asp:ListItem Text="Quaterly" >Quaterly</asp:ListItem>
                <asp:ListItem Text="HalfYearly" >HalfYearly</asp:ListItem>
                <asp:ListItem Text="Yearly" >Yearly</asp:ListItem> 
                </asp:RadioButtonList>
            <br /><br />

            <asp:Label runat="server" ID="perfertiming">Perfer Timing</asp:Label>
            <div id="PereferTiming" runat="server">
                <asp:CheckBox runat="server" ID="Morrning" Text="Morrning" />
                <asp:CheckBox runat="server" ID="Afternoon" Text="Afternoon" />
                <asp:CheckBox runat="server" ID="Evening" Text="Evening" />
                <asp:CheckBox runat="server" ID="Night" Text="Night" />
            </div>
            <asp:Button runat="server" ID="button" OnClick="submit" Text="Submit Me"/>
      
            <div runat="server" id="receipt"></div>
            
            
      </div>
    </form>
</body>
</html>